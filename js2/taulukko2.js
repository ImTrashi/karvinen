const funcs = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];
function myFunction() {
    for (let index = 0; index < funcs.length; index++) {
        const element = funcs[index];
        console.log(element)
    }
};
function tulostusI() {
    const tulos = funcs.filter(value => value.indexOf("i") > -1);
    console.log(tulos);
    for (let index = 0; index < funcs.length; index++) {
        const element = funcs[index];
        if(element.indexOf("i") > -1) {
            tulos.push(element);
        }
    }
    console.log(tulos);
}

function jarjesta() {
    console.log(funcs.sort());
}

function poistaEka() {
    console.log(funcs.shift());
    console.log(funcs);
}

function puske() {
    funcs.push("sipuli");
    console.log(funcs);
}